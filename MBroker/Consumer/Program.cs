﻿using FormatIndicator.ConcreteMessages;
using FormatIndicator.Format;
using MessageBrokers.Models.Consumers;
using System;

namespace Consumer
{
   class Program
   {
      static void Main(string[] args)
      {
         var listener = new RabbitMQSynchronousListener<MinionMessage>("ec2-52-204-52-122.compute-1.amazonaws.com", "user", "lGajuHj#2JYo6");
         listener.QueueName = "HearthstonePlays";

         Console.WriteLine("Listening for messages");
         while (true)
         {
            var message = listener.Listen();

            Console.WriteLine("Received message, Format:{0}", message.Format);

            var decorator = FormatUtils.DecorateMessage(message);
            var minion = decorator.Deserialize();

            Console.WriteLine("Message deserialized");
            Console.WriteLine("The oponent played: {0} with {1} Attack and {2} Health, and they payed {3} mana for it.", minion.Name, minion.Attack, minion.Health, minion.ManaCost);
            Console.WriteLine("The opponent's {0} activates: {1}", minion.Name, minion.Effect);
            Console.WriteLine("-----------------------------------------------------------");
         };
      }
   }
}

﻿using FormatIndicator.Format;

namespace FormatIndicator.Decorators
{
    public abstract class FormatDecorator<T> : FormatedMessage<T>
    {
        protected FormatedMessage<T> Message { get; set; }

        public override T Deserialize()
        {
            if(Message != null)
             return Message.Deserialize();

            return base.Deserialize();
        }

        public override string Serialize(T rawMessage)
        {
            if (Message != null)
                return Message.Serialize(rawMessage);
            return base.Serialize(rawMessage);
        }

        public virtual void SetMessage(FormatedMessage<T> message)
        {
            Message = message;
        }
    }
}

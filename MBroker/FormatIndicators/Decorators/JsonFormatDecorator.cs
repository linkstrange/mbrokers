﻿using Newtonsoft.Json;

namespace FormatIndicator.Decorators
{
    public class JsonFormatDecorator<T> : FormatDecorator<T>
    {
        public override T Deserialize()
        {
            return JsonConvert.DeserializeObject<T>(Message.MessagePayload);
        }

        public override string Serialize(T rawMessage)
        {
            Message.MessagePayload = JsonConvert.SerializeObject(rawMessage);
            return base.Serialize(rawMessage);
        }
    }
}

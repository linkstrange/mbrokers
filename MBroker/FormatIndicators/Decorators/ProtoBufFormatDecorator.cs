﻿using System;
using System.IO;
using ProtoBuf;

namespace FormatIndicator.Decorators
{
   public class ProtoBufFormatDecorator<T> : FormatDecorator<T>
   {
      public override T Deserialize()
      {
         var bytes = Convert.FromBase64String(Message.MessagePayload);
         return DeserializeInternal<T>(bytes);
      }

      public override string Serialize(T rawMessage)
      {
         Message.MessagePayload = Convert.ToBase64String(SerializeInternal(rawMessage));
         return base.Serialize(rawMessage);
      }

      private static byte[] SerializeInternal<T>(T instance)
      {
         byte[] b;
         using (var ms = new MemoryStream())
         {
            Serializer.Serialize(ms, instance);
            b = new byte[ms.Position];
            var fullB = ms.GetBuffer();
            Array.Copy(fullB, b, b.Length);
         }
         return b;
      }

      private static T DeserializeInternal<T>(byte[] instance)
      {
         T m;
         using (var ms = new MemoryStream(instance))
         {
            m = Serializer.Deserialize<T>(ms);
         }
         return m;
      }
   }
}

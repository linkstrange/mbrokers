﻿using System.IO;
using System.Xml.Serialization;

namespace FormatIndicator.Decorators
{
    public class XMLFormatDecorator<T> : FormatDecorator<T>
    {
        public override T Deserialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            var reader = new StringReader(Message.MessagePayload);
            var result = (T)serializer.Deserialize(reader);
            reader.Dispose();

            return result;
        }

        public override string Serialize(T rawMessage)
        {
            using (var stringwriter = new StringWriter())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, rawMessage);
                Message.MessagePayload = stringwriter.ToString();
            }

            return base.Serialize(rawMessage);
        }
    }
}

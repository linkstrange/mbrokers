﻿namespace FormatIndicator.Format
{
    public enum Format
    {
        Unknown = 0,
        Json = 1,
        XML = 2,
        Proto = 3
    }
}

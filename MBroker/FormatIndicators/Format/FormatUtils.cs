﻿using FormatIndicator.Decorators;
using System;

namespace FormatIndicator.Format
{
    public static class FormatUtils
    {
        public static FormatDecorator<T> DecorateMessage<T>(FormatedMessage<T> message)
        {
            switch (message.Format)
            {
                case Format.Json:
                    return GetJsonDecorator(message);
                case Format.XML:
                    return GetXMLDecorator(message);
                case Format.Proto:
               return GetProtoDecorator(message);
                case Format.Unknown:
                default:
                    throw new NotImplementedException();             
            }
        }

        private static FormatDecorator<T> GetXMLDecorator<T>(FormatedMessage<T> message)
        {
            var jsonDecorator = new XMLFormatDecorator<T>();
            jsonDecorator.SetMessage(message);
            return jsonDecorator;
        }

        private static FormatDecorator<T> GetJsonDecorator<T>(FormatedMessage<T> message)
        {
            var jsonDecorator = new JsonFormatDecorator<T>(); 
            jsonDecorator.SetMessage(message);
            return jsonDecorator;
        }

        private static FormatDecorator<T> GetProtoDecorator<T>(FormatedMessage<T> message)
        {
            var protoBufDecorator = new ProtoBufFormatDecorator<T>();
            protoBufDecorator.SetMessage(message);
            return protoBufDecorator;
        }   
   }
}

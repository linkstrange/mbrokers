﻿namespace FormatIndicator.Format
{
    public abstract class FormatedMessage<T> : Message<T>
    {
        public Format Format { get; set; }

        public override T Deserialize()
        {
            return default(T);
        }

        public override string Serialize(T rawMessage)
        {
            return MessagePayload;
        }
    }
}

﻿namespace FormatIndicator.Format
{
    public abstract class Message<T>
    {
        public string MessagePayload { get; set; }
        public abstract T Deserialize();
        public abstract string Serialize(T rawMessage);
    }
}

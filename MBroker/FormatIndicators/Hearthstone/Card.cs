﻿using ProtoBuf;

namespace FormatIndicator.Hearthstone
{
   [ProtoContract]
   [ProtoInclude(1000, typeof(Minion))]
   public class Card
   {
      [ProtoMember(1)]
      public string Name { get; set; }
      [ProtoMember(2)]
      public string Class { get; set; }
      [ProtoMember(3)]
      public string Effect { get; set; }
      [ProtoMember(4)]
      public int ManaCost { get; set; }
      [ProtoMember(5)]
      public Rarity Rarity { get; set; }
   }

   public enum Rarity
   {
      None,
      Common,
      Uncommon,
      Rare,
      Epic,
      Legendary
   }
}

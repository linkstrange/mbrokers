﻿using ProtoBuf;

namespace FormatIndicator.Hearthstone
{
   [ProtoContract]
   public class Minion : Card
   {
      [ProtoMember(1)]
      public int Attack { get; set; }
      [ProtoMember(2)]
      public int Health { get; set; }
      [ProtoMember(3)]
      public string Tribe { get; set; }
   }
}

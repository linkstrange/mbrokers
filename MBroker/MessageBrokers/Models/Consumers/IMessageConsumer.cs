﻿using System;

namespace MessageBrokers.Models.Consumers
{
    public interface IMessageConsumer<T>
    {
        T Listen();
    }
}

﻿using System;

namespace MessageBrokers.Models.Consumers
{
    public interface IWorkListener<T>
    {
        T ListenForWork();
    }
}

﻿namespace MessageBrokers.Models.Consumers
{
    public abstract class MessageConsumer<T> : IMessageConsumer<T>
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        protected MessageConsumer(string host, string username, string password)
        {
            HostName = host;
            UserName = username;
            Password = password;
        }

        public abstract T Listen();
    }
}

﻿namespace MessageBrokers.Models.Consumers
{
    public abstract class QueueBasedConsumer<T> : MessageConsumer<T>
    {
        public string QueueName { get; set; }
        public bool DeclareQueue { get; set; }
        public bool ListenToMessages { get; set; }

        protected QueueBasedConsumer(string host, string username, string password)
            : base(host, username, password)
        {
        }

        protected QueueBasedConsumer(string host, string username, string password, string queue)
            : base(host, username, password)
        {
            QueueName = queue;
        }

    }
}

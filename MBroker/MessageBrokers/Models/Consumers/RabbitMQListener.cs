﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using RabbitMQ.Client;

namespace MessageBrokers.Models.Consumers
{

    public abstract class RabbitMQListener<T> : QueueBasedConsumer<T>
    {
        private ConnectionFactory _factory;
        public bool QueueIsExclusive { get; set; }
        public bool AutoDeleteQueue { get; set; }
        public bool QueueIsDurable { get; set; }
        public Dictionary<string, object> Arguments { get; set; }

        protected RabbitMQListener(string host, string username, string password)
            : base(host, username, password)
        {
            QueueName = Guid.NewGuid().ToString();
            QueueIsDurable = false;
            QueueIsExclusive = false;
            AutoDeleteQueue = false;
            DeclareQueue = false;
            ListenToMessages = true;

            _factory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };
        }

        public override T Listen()
        {
            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    try
                    {
                        if (DeclareQueue)
                        {
                            channel.QueueDeclare(
                                             exclusive: QueueIsExclusive,
                                             queue: QueueName,
                                             durable: QueueIsDurable,
                                             autoDelete: AutoDeleteQueue,
                                            arguments: Arguments);
                            Console.WriteLine("Created queue {0}", QueueName);

                        }

                        if (!ListenToMessages)
                            return default(T);
                        
                        Console.WriteLine("Listening on {0}", QueueName);
                        return GetMessage(channel);
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine(e.Message);
                        return default(T);
                    }
                }
            }
        }

        protected abstract T GetMessage(IModel channel);
    }
}

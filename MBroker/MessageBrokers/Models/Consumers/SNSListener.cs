﻿using System;
using Amazon.SQS;
using Amazon.Runtime;
using Amazon;
using Amazon.SQS.Model;
using Amazon.SimpleNotificationService;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MessageBrokers.Models.Consumers
{
    public class SNSListener<T> : MessageConsumer<T>, IDisposable
    {
        private readonly BasicAWSCredentials _credentials;
        private readonly RegionEndpoint _endpoint;
        private readonly IList<string> _queuesCreated = new List<string>();
        public string Topic { get; set; }
        public string QueueName { get; set; }

        public SNSListener(string topicArn, string username, string password, RegionEndpoint endpoint, string queuename = "")
            : base(topicArn, username, password)
        {
            _credentials = new BasicAWSCredentials(username, password);
            _endpoint = endpoint;
            Topic = topicArn;
            QueueName = String.IsNullOrWhiteSpace(queuename) 
                              ? Guid.NewGuid().ToString() 
                              : queuename;
        }

        public void Dispose()
        {
            var sqsClient = new AmazonSQSClient(_credentials, _endpoint);
            var tasks = new List<Task>();
            foreach(var queue in _queuesCreated)
            {
                var task = sqsClient.DeleteQueueAsync(new DeleteQueueRequest
                {
                    QueueUrl = queue,
                });
                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());
        }

        public override T Listen()
        {
            var sqsClient = new AmazonSQSClient(_credentials, _endpoint);
            var queueURL = sqsClient.CreateQueueAsync(new CreateQueueRequest
            {
                QueueName = this.QueueName,
            }).Result.QueueUrl;

            _queuesCreated.Add(QueueName);
            // subscribe to topic
            var snsClient = new AmazonSimpleNotificationServiceClient(_credentials, _endpoint);
            snsClient.SubscribeQueueAsync(Topic,sqsClient, queueURL).Wait();

            var message = String.Empty;

            while (String.IsNullOrWhiteSpace(message))
            {
                // Get the message from the queue.
                var messages = sqsClient.ReceiveMessageAsync(new ReceiveMessageRequest
                {
                    QueueUrl = queueURL,
                    WaitTimeSeconds = 0,
                    MaxNumberOfMessages = 1,
                }).Result.Messages;

                if (messages != null && messages.Any())
                {
                    var first = messages.FirstOrDefault();
                    message = first.Body;
                    sqsClient.DeleteMessageAsync(new DeleteMessageRequest
                    {
                        ReceiptHandle = first.ReceiptHandle,
                        QueueUrl = queueURL,
                    }).Wait();
                }
            }

            Console.WriteLine("[<-] Received SNS");
            return JsonConvert.DeserializeObject<T>(message); 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace MessageBrokers.Models.Producers
{
    public enum RabbitMQPublishOptions
    {
        None = 0,
        Queue = 1,
        Topic = 2,
    }

    public class RabbitMQPublisher : QueueBasedProducer
    {
        private readonly ConnectionFactory _factory;
        public RabbitMQPublishOptions PublishTo { get; set; }
        public bool QueueIsExclusive { get; set; }
        public bool AutoDeleteQueue { get; set; }
        public bool QueueIsDurable { get; set; }
        public Dictionary<string, object> Arguments { get; set; }

        public RabbitMQPublisher(string host, string username, string password)
            : base(host, username, password)
        {
            PublishTo = RabbitMQPublishOptions.Queue;
            QueueIsDurable = false;
            QueueIsExclusive = false;
            AutoDeleteQueue = false;
            DeclareQueue = true;
            PublishMessage = true;

            _factory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };
        }

        public override void Send<T>(T message) 
        {
            if (message == null)
                return;

            if (String.IsNullOrWhiteSpace(Topic) && String.IsNullOrWhiteSpace(QueueName))
                throw new ArgumentException("Topic and QueueName can't both be null or empty");

            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var toSend = JsonConvert.SerializeObject(message);
                    var body = Encoding.UTF8.GetBytes(toSend);

                    if (DeclareQueue)
                        CreateQueue(channel);
                    
                    if(PublishMessage)
                        Publish(channel, body);
                }
            }
        }

        private void CreateQueue(IModel channel)
        {
            channel.QueueDeclare(
                             exclusive: QueueIsExclusive,
                             queue: QueueName,
                             durable: QueueIsDurable,
                             autoDelete: AutoDeleteQueue,
                            arguments: Arguments);

            Console.WriteLine("Created queue {0}", QueueName);
        }

        private void Publish(IModel channel, byte[] body)
        {
            switch(PublishTo)
            {
                case RabbitMQPublishOptions.Queue:
                    channel.BasicPublish(exchange: "",
                                         routingKey: QueueName,
                                         basicProperties: null,
                                         body: body);
                    break;

                case RabbitMQPublishOptions.Topic:
                    channel.BasicPublish(exchange: Topic,
                                         routingKey: "",
                                         basicProperties: null,
                                         body: body);
                    break;

                default:
                    throw new NotImplementedException(PublishTo.ToString());
            }
        }
    }
}

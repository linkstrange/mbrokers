﻿using System;
using System.Collections.Generic;

namespace MessageBrokers.Models.Request
{
    public class APIRequest
    {
        public int Version { get; set; }
        public List<string> Providers { get; set; }
        public string Payload { get; set; }
        public string Id { get; set; }
        public DateTime DtSent { get; set; }
    }
}

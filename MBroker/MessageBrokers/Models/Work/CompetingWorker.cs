﻿namespace MessageBrokers.Models.Work
{
    //http://www.enterpriseintegrationpatterns.com/patterns/messaging/CompetingConsumers.html
    //Notifies Correlation Id to Aggregator before processing work
    public abstract class CompetingWorker : IWorkProcessor
    {
        public object ProcessWork(object message)
        {
            if (CanHandleWork(message))
                return ProcessWorkload(message);

            return null;
        }

        protected abstract bool CanHandleWork(object message);
        protected abstract object ProcessWorkload(object message);
    }
}

﻿using System;

namespace MessageBrokers.Models.Work
{
    public interface IWorkProcessor
    {
        object ProcessWork(object message);
    }
}

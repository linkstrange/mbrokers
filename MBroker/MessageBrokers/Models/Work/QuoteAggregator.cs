﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;
using MessageBrokers.Models.Response;
using MessageBrokers.Models.Storage;

namespace MessageBrokers.Models.Work
{
    public class QuoteAggregator<T> : AggregatorWorker 
        where T : APIRequest
    {
        private readonly QueueBasedConsumer<T> _listener;
        private readonly Random RNG = new Random(DateTime.Now.Millisecond);
        private readonly IKeyValueStorage _storage;

        public QuoteAggregator(int timeoutInMilliseconds, QueueBasedConsumer<T> listener, IKeyValueStorage storage)
            : base(timeoutInMilliseconds)
        {
            _storage = storage;
            _listener = listener;
        }

        protected override object AggregateWork(object message, int timeout)
        {
            if (message == null)
                return null;

            var task = Task.Run<AggregatorResponse>(() =>
            {
                var r = (T)message;
                var response = new AggregatorResponse() { Id = r.Id };
                _listener.QueueName = r.Id;

                try
                {
                    int counter = 0;

                    while (counter == 0)
                    {
                        int.TryParse(_storage.Get(r.Id).ToString(), out counter);
                        Thread.Sleep(1);
                    }

                    while (counter > 0)
                    {
                        var work = (T)_listener.Listen(); //Blocking
                        counter = (int)_storage.Decrement(r.Id);
                        _listener.DeclareQueue = false;

                        if (work is null)
                            continue;

                        var id = work.Id.Split('-')[RNG.Next(4)];
                        response.AddResponse(id[RNG.Next(id.Length)].ToString());
                    }

                    return response;

                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                    return null;
                }
                finally
                {
                    _storage.Delete(r.Id);
                }

            });

            if (!task.Wait(timeout))
            {
                Console.Out.WriteLine("[x] Timeout for {0}", ((T)message).Id);
                return null;
            }

            return task.Result;
        }
    }
}

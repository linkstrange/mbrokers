﻿using System;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;
using MessageBrokers.Models.Work;
using MessageBrokers.Models.WorkListener;
using MessageBrokers.Models.WorkResult;

namespace MessageBrokers.Models.WorkDispatcher
{
    public class StandardDispatcher : Dispatcher
    {
        private readonly Func<IWorkListener> _listener;
        private readonly Func<IWorkProcessor> _work;
        private readonly Func<IWorkResultPublisher> _result;

        public StandardDispatcher(Func<IWorkListener> listener, Func<IWorkProcessor> work, Func<IWorkResultPublisher> result)
        {
            _listener = listener;
            _work = work;
            _result = result;
        }

        protected override object ListenForWork()
        {
            var work = _listener().ListenForWork();

            return work;
        }

        protected override object ProcessWork(object work)
        {
            return _work().ProcessWork(work);
        }

        protected override void PublishResult(object result)
        {
            _result().PublishResult(result);
        }
    }
}

﻿using System;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;

namespace MessageBrokers.Models.WorkListener
{
    public class AggregatorListener<T> : IWorkListener
    {
        private readonly IMessageConsumer<T> _listener;

        public AggregatorListener(IMessageConsumer<T> listener)
        {
            _listener = listener;
        }

        public object ListenForWork()
        {
            return _listener.Listen();
        }
    }
}

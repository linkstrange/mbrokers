﻿using System;
using MessageBrokers.Models.Producers;
using MessageBrokers.Models.Response;

namespace MessageBrokers.Models.WorkResult
{
    public class PublishAggregatedResult<T> : IWorkResultPublisher
        where T : AggregatorResponse
    {
        private readonly QueueBasedProducer _publisher;

        public PublishAggregatedResult(QueueBasedProducer publisher)
        {
            _publisher = publisher;
        }

        public void PublishResult(object message)
        {            
            if (message is null)
                return;

            var o = (T)message;

            var format = "{prefix}-{Id}".Replace("{prefix}", "aggregated").Replace("{Id}", o.Id);
            _publisher.QueueName = format;
            _publisher.Send(message);
            Console.WriteLine("  [->] Published {0}", format);
        }
    }
}

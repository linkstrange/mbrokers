﻿using System;
using System.Threading;

namespace MessageBrokers.Models.WorkResult
{
    public class WriteResultToConsole : IWorkResultPublisher
    {
        public void PublishResult(object message)
        {
            if (message == null)
                return;
            
            Console.WriteLine("[->] provider {0} processed", message);
        }
    }
}

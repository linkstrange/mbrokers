﻿using FormatIndicator.ConcreteMessages;
using FormatIndicator.Format;
using FormatIndicator.Hearthstone;
using MessageBrokers.Models.Producers;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Producer
{
   class Program
   {
      static Random rng = new Random();

      static void Main(string[] args)
      {
         var rabbitPublisher = new RabbitMQPublisher("ec2-52-204-52-122.compute-1.amazonaws.com", "user", "lGajuHj#2JYo6");

         rabbitPublisher.QueueName = "HearthstonePlays";
         rabbitPublisher.Topic = "HS";

         Console.WriteLine("Generating random messages: ");
         while (true)
         {
            var message = GenerateMessage();

            Console.WriteLine("Generated message, Format:{0}", message.Format);
            Console.WriteLine("Payload:{0}", message.MessagePayload);


            Console.WriteLine("Sending Message...");
            rabbitPublisher.Send(message);

            Console.WriteLine("Message Sent: Waiting 5 seconds before new message is sent");
            Console.WriteLine("-----------------------------------------------------------");
            Thread.Sleep(5000);
         }

      }


      public static MinionMessage GenerateMessage()
      {

         var minionsList = new List<Minion>
            {
                new Minion
                {
                   Attack = 5,
                   Health = 7,
                   Class = "Mage",
                   Effect = "Whenever you cast a spell add a 'Fireball' spell to your hand",
                   ManaCost = 7,
                   Name = "Archmage Antonidas",
                   Tribe = "none",
                   Rarity = Rarity.Legendary
                },
                new Minion
                {
                   Attack = 8,
                   Health = 8,
                   Class = "Mage",
                   Effect = "Battlecry: Summon two 0/1 Frozen Champions",
                   ManaCost = 8,
                   Name = "Sindragosa",
                   Tribe = "Dragon",
                   Rarity = Rarity.Legendary
                },
                new Minion
                {
                   Attack = 8,
                   Health = 8,
                   Class = "Neutral",
                   Effect = "Battlecry: Set a hero's remaining health to 15",
                   ManaCost = 9,
                   Name = "Alextrasza",
                   Tribe = "Dragon",
                   Rarity = Rarity.Legendary
                },
                new Minion
                {
                   Attack = 4,
                   Health = 12,
                   Class = "Neutral",
                   Effect = "Spell Damage +5",
                   ManaCost = 9,
                   Name = "Malygos",
                   Tribe = "Dragon",
                   Rarity = Rarity.Legendary
                },
                new Minion
                {
                   Attack = 2,
                   Health = 2,
                   Class = "Mage",
                   Effect = "Deathrattle: Return this to your hand as a 6/6 that costs (6)",
                   ManaCost = 2,
                   Name = "Pyros",
                   Tribe = "Elemental",
                   Rarity = Rarity.Legendary
                },
            };

         var minionIndex = rng.Next(4);
         var formatType = rng.Next(1, 4);

         var minionMessage = new MinionMessage();

         minionMessage.Format = (Format)formatType;

         var decorator = FormatUtils.DecorateMessage(minionMessage);
         decorator.Serialize(minionsList[minionIndex]);

         return minionMessage;
      }
   }
}
